import os
import json
import xmltodict
from pathlib import Path
from uuid import uuid4
from asgiref.sync import sync_to_async

from django.http.response import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view

from .utils import write_to_xml, conditionally_return_class, batch_save_entries
from .validators import validate_json_from_schema, validate_json_to_xml, validate_xml


@api_view(["GET", "POST"])
def get_list(request, entity) -> JsonResponse:

    print(entity)

    if request.method == "GET":
        entities = conditionally_return_class(entity).scan()
        return_list = []
        return_dict = {}
        for item in entities:
            return_dict['_id'] = item._id
            return_dict[entity] = item.data[entity]
            return_list.append(return_dict.copy())
        print(return_dict)

        return JsonResponse(return_list, safe=False)

    if request.method == "POST":
        entities_data = JSONParser().parse(request)
        _id = str(uuid4())
        model_instance = conditionally_return_class(entity)(_id, None, data=entities_data)

        if not validate_json_from_schema(entity, json.dumps(entities_data)):
            return JsonResponse({"message":"Goofed the json fam"}, status=status.HTTP_400_BAD_REQUEST)
            # TODO: Fill in DETAILED error response

        xml_validation_result = validate_json_to_xml(entity, json.dumps(entities_data))
        if xml_validation_result:
            return JsonResponse(xml_validation_result, status=status.HTTP_400_BAD_REQUEST)

        model_instance.save()
        return JsonResponse(model_instance.data, status=status.HTTP_201_CREATED)

@api_view(["POST"])
def upload_entity(request) -> JsonResponse:
    """
    Takes an file in the POST data, reads it, checks it against the schema for the correct entity 
    then parses it into JSON. After that, it iterates through the JSON and creates new table records.
    """

    if request.FILES == {}:
        return JsonResponse({'message': 'No files found'}, status=status.HTTP_400_BAD_REQUEST)

    for file in request.FILES:
        xml_content = request.FILES[file].read().decode('utf-8')
        xml_dict = xmltodict.parse(xml_content)
        xml_validation_result = validate_xml(list(xml_dict.keys())[0], xml_content)
    
    if xml_validation_result:
        return JsonResponse(xml_validation_result, status=status.HTTP_400_BAD_REQUEST)
    else:
        sync_to_async(batch_save_entries(xml_dict))
        return JsonResponse({"message":"Upload Successful"}, status=status.HTTP_202_ACCEPTED)
    

@api_view(["GET", "PUT", "DELETE"])
def entity_by_id(request, entity: str, entity_id: str) -> JsonResponse:

    print(entity)

    if request.method == "GET":
        return_dict = {}
        try:
            entities = conditionally_return_class(entity).get(entity_id)
        except conditionally_return_class(entity).DoesNotExist:
            return JsonResponse(
                {"message": f"{entity} with ID {entity_id} not found."},
                status=status.HTTP_404_NOT_FOUND,
            )
        return_dict['_id'] = entities._id
        return_dict[entity] = entities.data[entity]
        return JsonResponse(return_dict, safe=False)
    if request.method == "PUT":
        return_dict = {}
        try:
            entities = conditionally_return_class(entity).get(entity_id)
        except conditionally_return_class(entity).DoesNotExist:
            return JsonResponse(
                {"message": f"{entity} with ID {entity_id} not found."},
                status=status.HTTP_404_NOT_FOUND,
            )

        entities_data = JSONParser().parse(request)
        if not validate_json_from_schema(entity, json.dumps(entities_data)):
            return JsonResponse({}, status=status.HTTP_400_BAD_REQUEST)
            # TODO: Fill in DETAILED error response

        if validate_json_to_xml(entity, json.dumps(entities_data)):
            return JsonResponse({}, status=status.HTTP_400_BAD_REQUEST)
            # TODO: Fill in DETAILED error response
        entities.data.set(entities_data)
        entities.save()
        return_dict[entities._id] = entities.data
        return JsonResponse(return_dict, status=status.HTTP_202_ACCEPTED)

    if request.method == "DELETE":
        try:
            entities = conditionally_return_class(entity).get(entity_id)
        except conditionally_return_class(entity).DoesNotExist:
            return JsonResponse(
                {"message": f"{entity} with ID {entity_id} not found."},
                status=status.HTTP_404_NOT_FOUND,
            )
        entities.delete()
        return JsonResponse(
            {"message": f"{entity} with ID {entity_id} deleted"},
            safe=False,
            status=status.HTTP_200_OK,
        )


@api_view(["GET", "HEAD"])
def download_entity(request, entity_name: str):
    """Downloads the entity by the given name..."""
    print(entity_name)

    # TODO: Check if such entity_name is correct?.. I.e., is there such in our database?

    # Double check file[path]s...
    Path("/tmp/").mkdir(exist_ok=True)
    filename = f'{entity_name}.xml'
    filepath = os.path.join('/tmp', filename)
    if os.path.isfile(filepath):
        os.remove(filepath)

    # Compose and format the data load...
    entities = conditionally_return_class(entity_name).scan()
    dataload = []
    for entity in entities:
        dataload.append(entity.data.copy())
    # print(str(dataload))

    # Put the data into an XML-file...
    write_to_xml(filepath=filepath, entity_name=entity_name, data=dataload)

    # Compose response...
    response = HttpResponse(open(filepath, "rb"), content_type="application/force-download")
    response["X-Accel-Redirect"] = f'/tmp/{filename}'
    response["Content-Disposition"] = f'attachment; filename="{filename}"'
    return response
