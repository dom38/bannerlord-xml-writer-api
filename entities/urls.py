from django.urls import path
from entities import views

urlpatterns = [
    path("upload/", views.upload_entity),
    path("<str:entity>/", views.get_list),
    path("<str:entity_name>/export/", views.download_entity),
    path("<str:entity>/<str:entity_id>/", views.entity_by_id),
]
