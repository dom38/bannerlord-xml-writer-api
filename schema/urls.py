from django.urls import path
from schema import views

urlpatterns = [
    path("<str:schema_name>/", views.display_schema),
    path("", views.list_schema),
]
