from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, JSONAttribute
)

from django.conf import settings

class Entity(Model):
    class Meta:
        region = 'eu-west-1'
        billing_mode = 'PAY_PER_REQUEST'
    
    _id = UnicodeAttribute(hash_key=True)
    data = JSONAttribute()
    

class BasicCultures(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_BasicCultures'


class Concepts(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Concepts'


class CraftingPieces(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_CraftingPieces'


class CraftingTemplates(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_CraftingTemplates'


class Factions(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Factions'


class Heroes(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Heroes'


class ItemModifierGroups(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_ItemModifierGroups'


class ItemModifiers(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_ItemModifiers'


class Items(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Items'


class Kingdoms(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Kingdoms'


class LocationComplexTemplates(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_LocationComplexTemplates'


class Monsters(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Monsters'


class MPCharacters(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_MPCharacters'


class MPClassDivisions(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_MPClassDivisions'


class NPCCharacters(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_NPCCharacters'

class partyTemplates(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_partyTemplates'


class Settlements(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Settlements'


class SiegeEngineTypes(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_SiegeEngineTypes'


class Scales(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_Scales'


class SPCultures(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_SPCultures'


class WorkshopTypes(Entity):
    class Meta(Entity.Meta):
        table_name = f'{settings.ENVIRONMENT}_bannerlord_xml_writer_WorkshopTypes'
