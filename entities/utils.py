"""
Self-explanatory module name should already give you a hint that it is used for utility functionality...
"""
from pathlib import Path
from typing import List, Union, Any, Optional, Dict
from uuid import uuid4
from django.db import models
import importlib

from xmltodict import unparse

MODELS_MODULE = importlib.import_module('.models', package='entities')

def write_to_xml(filepath: Union[str, Path], entity_name: str, data: List[Any],
                 **unparse_kwargs: Optional[Dict[str, Any]]) -> None:
    """
    Writes the given data to the file-stream wrapped with XML-like structure...
    :param filepath: A filepath to open a file-stream for Write-mode...
    :param entity_name:
    :param data: All the textable data you want to put in your XML...
    :param unparse_kwargs: All the keyword arguments you might want to pass to `xmltodict.unparse()`...
    :return: Nothing...
    """
    with open(filepath, 'w') as file_stream:
        file_stream.write('<?xml version="1.0" encoding="utf-8"?>')
        file_stream.write(f'<{entity_name}>')
        for item in data:
            file_stream.write(unparse(item[entity_name], full_document=False, **unparse_kwargs))
        file_stream.write(f'</{entity_name}>')

def conditionally_return_class(entity: str) -> Optional[models.Model]:
    """
    Conditionally returns the Model instance of a required Item
    :param entity: A str that corresponds to an existing Model entity
    :return: models.Model instance of desired Model
    """
    
    try:
        imported_entity = getattr(MODELS_MODULE, entity)
    except KeyError:
        return None
    if not imported_entity.exists():
        imported_entity.create_table(billing_mode='PAY_PER_REQUEST', wait=True)
    return imported_entity

def batch_save_entries(xml_dict: dict) -> None:
    """
    Asynchronous function that batch posts XML data to the Database
    :param xml_dict: A Dict of parsed XML that has been transformed by the view
    :return: None
    """
    for entity in xml_dict.keys():
                for object in xml_dict[entity].keys():
                    for data in xml_dict[entity][object]:
                        return_value = {
                            f'{entity}':{
                                f'{object}' : data
                            }
                        }
                        _id = str(uuid4())
                        model_instance = conditionally_return_class(entity)(_id, None, data=return_value)
                        model_instance.save()
                        print('Saving object')
