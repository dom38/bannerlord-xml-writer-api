from django.shortcuts import render

from django.http.response import JsonResponse

from rest_framework.decorators import api_view
import os
import json


@api_view(["GET"])
def list_schema(request):
    sanitised_entries = []
    entries = os.listdir("schema/schemas/")
    for filename in entries:
        if os.path.splitext(filename)[1] == ".json":
            sanitised_entries.append(os.path.splitext(filename)[0])
    print("SchemaList")
    return JsonResponse(sanitised_entries, safe=False)


@api_view(["GET"])
def display_schema(request, schema_name):
    print(schema_name)
    return JsonResponse(
        json.loads(open(f"schema/schemas/{schema_name}.json", "r").read())
    )
