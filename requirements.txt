django==3.1.3
django-cors-headers == 3.5.0
djangorestframework == 3.12.1
psycopg2-binary
jsonschema
gunicorn
xmltodict
xmlschema
zappa
importlib-metadata
pynamodb
typing_extensions
