import os
import sys

from django.test import TestCase

# Create your tests here.
from settings import BASE_DIR
from .views import conditionally_return_class, conditionally_return_serializer
from .serializers import *
from .models import *


SCHEMAS_DIR_PATH = os.path.join(BASE_DIR, 'schema', 'schemas')
ENTITY_NAMES = [filename.replace('.xsd', '') for filename in os.listdir(SCHEMAS_DIR_PATH) if filename.endswith('.xsd')]


class ViewsTC(TestCase):

    def test_conditionally_return_class(self):
        for entity in ENTITY_NAMES:
            with self.subTest("Model subtest", entity=entity):
                result = conditionally_return_class(entity)
                gold = getattr(sys.modules[__name__], entity)
                self.assertEqual(result, gold)

    def test_conditionally_return_serializer(self):
        for entity in ENTITY_NAMES:
            with self.subTest("Serializer subtest", entity=entity):
                result = conditionally_return_serializer(entity)

                gold = getattr(sys.modules[__name__], f'{entity}Serializer')
                self.assertEqual(result, gold)
