## How do I run the API application locally?

REQUIREMENT: AWS credentials exported in your shell: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

1. Install [Python 3.8](https://www.python.org/)
2. Clone this repo
3. In the root, run `pip install -r requirements.txt`
4. In the frontend folder, run `python manage.py runserver`, then navigate to `http://localhost:8000/`

You can interact with the API using tools like Postman.
