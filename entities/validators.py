import json
import os

import jsonschema
from jsonschema import validate
import xmlschema
import ast

from .utils import write_to_xml


def validate_json_from_schema(entity: str, data: str) -> bool:
    """
    Validates the given JSON data against the given schema
    :param entity: JSON-schema's filename (located in 'schema/schemas/') to validate against
    :param data: JSON-data to validate
    :return: True if valid, False otherwise
    """
    schema = json.load(open(f"schema/schemas/{entity}.json"))
    print(data)
    json_data = json.loads(data)
    try:
        validate(instance=json_data, schema=schema)
    except jsonschema.exceptions.ValidationError as err:
        return False
    return True


def validate_json_to_xml(entity: str, data: str):
    """
    Validates the given JSON-data as XML
    :param entity: XML schema's name (located in 'schema/schemas/') to validate against
    :param data: JSON data to validate
    :return: Nothing if successful, full error if not
    """
    schema = xmlschema.XMLSchema(f"schema/schemas/{entity}.xsd", base_url="schema/schemas/")
    filename = f"/tmp/{entity}TEST.xml"
    data_to_load = ast.literal_eval(data)
    write_to_xml(filepath=filename, entity_name=entity, data=[data_to_load])
    try:
        schema.validate(filename)
    except xmlschema.XMLSchemaValidationError as err:
        return {
            'message': f'XML Parsing issue with entity {entity}',
            'data': data,
            'validationError': str(err)
        }
    finally:
        os.remove(filename)
    return None

def validate_xml(entity: str, data: str):
    """
    Validates the given XML string as valid XML
    :param entity: XML schema's name (located in 'schema/schemas/') to validate against
    :param data: XML data to validate
    :return: Nothing if successful, full error if not
    """
    schema = xmlschema.XMLSchema(f"schema/schemas/{entity}.xsd", base_url="schema/schemas/")
    filename = f"/tmp/{entity}TEST.xml"
    file = open(filename, 'w')
    file.write(data)
    file.close()
    try:
        schema.validate(filename)
    except xmlschema.XMLSchemaValidationError as err:
        return {
            'message': f'XML Parsing issue with entity {entity}',
            'data': data,
            'validationError': str(err)
        }
    finally:
        os.remove(filename)
    return None
